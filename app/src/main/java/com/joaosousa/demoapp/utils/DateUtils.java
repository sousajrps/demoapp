package com.joaosousa.demoapp.utils;

/**
 * Created by jrpso on 05/10/2017.
 */

import android.text.TextUtils;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * <prev>
 * Created by João Sousa on 2017-10-04.
 * ebankIT
 * joao.sousa@ebankit.com
 * </prev>
 *
 * @version 2017.10.04
 */
public class DateUtils {
    private static final String defaultPattern = "yyyy-MM-dd";

    /**
     * Returns the number of years difference from current date
     *
     * @param date Reference date to be compared to current date (The date should be in the format:
     *             yyyy-MM-dd)
     */
    public static int getNumberOfYears(String date) {
        return getNumberOfYears(date, defaultPattern);
    }

    /**
     * Returns the numbers of years difference from the current date
     *
     * @param date    Reference date to be compared to current date
     * @param pattern The joda time pattern for the reference date
     */
    public static int getNumberOfYears(String date, String pattern) {
        if (TextUtils.isEmpty(date) || TextUtils.isEmpty(pattern)) {
            return 0;
        }
        try {
            DateTimeFormatter formatterOutput1 = DateTimeFormat.forPattern(pattern);
            DateTime dateTime = formatterOutput1.parseDateTime(date);
            return Years.yearsBetween(dateTime.toLocalDate(),
                    DateTime.now().toLocalDate()).getYears();
        } catch (Exception e) {
            Log.e(DateUtils.class.getSimpleName(), e.getMessage(), e);
            return 0;
        }

    }

    /**
     * Returns the numbers of days difference from the current date
     *
     * @param date Reference date to be compared to current date (The date should be in the format:
     *             yyyy-MM-dd)
     */
    public static int getNumberOfDaysRemaining(String date) {
        return getNumberOfDaysRemaining(date, defaultPattern);
    }

    /**
     * Returns the number of days difference from current date
     *
     * @param date    Reference date to be compared to current date
     * @param pattern The joda time pattern for the reference date
     */
    public static int getNumberOfDaysRemaining(String date, String pattern) {
        if (TextUtils.isEmpty(date) || TextUtils.isEmpty(pattern)) {
            return 0;
        }
        try {
            DateTimeFormatter formatterOutput1 = DateTimeFormat.forPattern(pattern);
            DateTime dateTime = formatterOutput1.parseDateTime(date);
            return Days.daysBetween(dateTime.toLocalDate(), DateTime.now().toLocalDate()).getDays();
        } catch (Exception e) {
            Log.e(DateUtils.class.getSimpleName(), e.getMessage(), e);
            return 0;
        }
    }
}
