package com.joaosousa.demoapp.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.DisplayMetrics;

import com.joaosousa.demoapp.R;

/**
 * Created by jrpso on 05/10/2017.
 */

public class Utils {
    /**
     * Return the number of columns for a recyclerView GridLayout
     *
     * @param context Activity context
     * @param width   width of a single item of the gridLayout
     * @return (int) number of items that fir the screen
     */
    public static int getNumberOfColumnsForWidth(Context context, int width) {
        if (context == null || width <= 0) {
            return 0;
        }
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / width);
        return noOfColumns;
    }

    /**
     * Return the color to be applied to the Compliance text according to its value
     * @param compliance character
     * @return
     */
    public static int getComplianceColor(String compliance) {
        if (TextUtils.isEmpty(compliance)) {
            return R.color.coral_pink;
        }
        switch (compliance.toLowerCase()) {
            case "a":
            case "b":
                return R.color.sgreen;
            case "c":
            case "d":
                return R.color.sorenge;
            case "e":
            case "f":
                return R.color.coral_pink;
            default:
                return R.color.coral_pink;
        }
    }

    /**
     * Returns the id of the drawable corresponding to the patient's name
     *
     * @param name
     * @return
     */
    public static int getPatientPhoto(String name) {
        if (TextUtils.isEmpty(name)) {
            return R.drawable.photofemale_3;
        }
        switch (name) {
            case "Jack Nelson":
                return R.drawable.jack;
            case "Hannah Miller":
                return R.drawable.hannah_miller;
            case "John Douglas":
                return R.drawable.john;
            case "Claire Hughes":
                return R.drawable.claire_hughes;
            case "Steven Howard":
                return R.drawable.steven;
            case "Anna Smith":
                return R.drawable.anna;
            case "Mary Smith":
                return R.drawable.mary;
            default:
                return R.drawable.photofemale_3;
        }
    }
}
