package com.joaosousa.demoapp;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by jrpso on 05/10/2017.
 */

public class ApplicationClass extends Application {
    /**
     * Variables
     */
    private static ApplicationClass instance;

    /**
     * @return
     */
    public static ApplicationClass getInstance() {
        return instance;
    }

    /**
     *
     * @return
     */
    public static boolean hasNetwork() {
        return instance.checkIfHasNetwork();
    }

    /**
     *
     */
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(Configs.DEFAULT_FONT)
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    /**
     *
     * @return
     */
    private boolean checkIfHasNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
