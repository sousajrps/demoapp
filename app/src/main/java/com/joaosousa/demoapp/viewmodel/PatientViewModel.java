package com.joaosousa.demoapp.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.joaosousa.demoapp.remote.model.Patient;
import com.joaosousa.demoapp.remote.repository.PatientRepository;

import java.util.List;

/**
 * Created by jrpso on 03/10/2017.
 */

public class PatientViewModel extends AndroidViewModel {
    /**
     * Variables
     */
    private final LiveData<List<Patient>> patientListObservable;

    /**
     * @param application
     * @param user
     */
    public PatientViewModel(Application application, final String user) {
        super(application);

        patientListObservable = PatientRepository.getInstance().getPatientList(user);
    }

    /**
     *
     * @return
     */
    public LiveData<List<Patient>> getPatientListObservable() {
        return patientListObservable;
    }

    /**
     *
     */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application application;

        private final String user;

        public Factory(@NonNull Application application, String user) {
            this.application = application;
            this.user = user;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new PatientViewModel(application, user);
        }
    }
}
