package com.joaosousa.demoapp.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.joaosousa.demoapp.local.User;
import com.joaosousa.demoapp.local.UserDatabase;

import java.util.List;

/**
 * Created by jrpso on 06/10/2017.
 */

public class UserViewModel extends AndroidViewModel {

    /**
     * Variables
     */
    private final LiveData<List<User>> userList;
    private UserDatabase userDatabase;

    /**
     * @param application
     */
    public UserViewModel(Application application) {
        super(application);

        userDatabase = UserDatabase.getDatabase(application);

        userList = userDatabase.userDao().getUsers();
    }

    /**
     *
     * @return
     */
    public LiveData<List<User>> getUsers() {
        return userList;
    }

    /**
     *
     * @param user
     */
    public void deleteUser(User user) {
        new deleteAsyncTask(userDatabase).execute(user);
    }

    /**
     *
     * @param user
     */
    public void insertUser(User user) {
        new insertAsyncTask(userDatabase).execute(user);
    }

    /**
     *
     */
    private static class deleteAsyncTask extends AsyncTask<User, Void, Void> {

        private UserDatabase db;

        deleteAsyncTask(UserDatabase userDatabase) {
            db = userDatabase;
        }

        @Override
        protected Void doInBackground(final User... params) {
            db.userDao().deleteUser(params[0]);
            return null;
        }

    }

    /**
     *
     */
    private static class insertAsyncTask extends AsyncTask<User, Void, Void> {

        private UserDatabase db;
        insertAsyncTask(UserDatabase userDatabase) {
            db = userDatabase;
        }

        @Override
        protected Void doInBackground(final User... params) {
            db.userDao().insertUser(params[0]);
            return null;
        }

    }

}
