package com.joaosousa.demoapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joaosousa.demoapp.R;
import com.joaosousa.demoapp.remote.model.Patient;
import com.joaosousa.demoapp.utils.DateUtils;
import com.joaosousa.demoapp.utils.Utils;
import com.joaosousa.demoapp.view.PatientDetailDialogFragment;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by jrpso on 04/10/2017.
 */

public class PatientAdapter extends RecyclerView.Adapter<PatientAdapter.PatientViewHolder> {
    /**
     * Variables
     */
    private List<Patient> patientList;
    private String selectedPatientName;
    private Context context;

    /**
     * Views
     */
    private View oldView;

    /**
     * Patient Adapter constructor
     *
     * @param context             Activity context
     * @param patientList         List of patients to be displayed
     * @param selectedPatientName Name of the patient to set as selected
     */
    public PatientAdapter(Context context, List<Patient> patientList, String selectedPatientName) {
        this.context = context;
        this.patientList = patientList;
        this.selectedPatientName = selectedPatientName;
    }

    /**
     *
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public PatientAdapter.PatientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.adapter_patient_card_view, parent, false);
        PatientViewHolder viewHolder = new PatientViewHolder(v);
        return viewHolder;
    }

    /**
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(PatientAdapter.PatientViewHolder holder, int position) {
        Patient patient = patientList.get(position);
        holder.patientPhotoCIV.setImageResource(Utils.getPatientPhoto(patient.getName()));

        holder.patientNameTV.setText(patient.getName().toString());

        holder.patientAgeTV.setText(DateUtils.getNumberOfYears(patient.getBirthDate()) + " " + context.getString(R.string.patient_item_years_old));

        if (patient.getHasPrescription() == 1) {
            holder.patientLastSessionLL.setVisibility(View.VISIBLE);
            holder.patientComplianceLL.setVisibility(View.VISIBLE);
            holder.patientNoPrescriptionTV.setVisibility(View.GONE);
            holder.patientComplianceTV.setText(patient.getCompliance());
            holder.patientComplianceTV.setTextColor(context.getResources().getColor(Utils.getComplianceColor(patient.getCompliance())));


            holder.patientLastSessionTV.setVisibility(View.VISIBLE);
            String lastSession;
            if (!TextUtils.isEmpty(patient.getLastSession()) && !patient.getLastSession().equalsIgnoreCase("null")) {
                int days = DateUtils.getNumberOfDaysRemaining(patient.getLastSession());
                if (days == 0) {
                    lastSession = context.getString(R.string.patient_item_today);
                } else if (days == 1) {
                    lastSession = String.valueOf(days) + " " + context.getString(R.string.patient_item_day_ago);
                } else {
                    lastSession = String.valueOf(days) + " " + context.getString(R.string.patient_item_days_ago);
                }
            } else {
                lastSession = "-";
            }
            holder.patientLastSessionTV.setText(lastSession);

            holder.patientNoPrescriptionTV.setVisibility(View.GONE);
        } else {
            holder.patientLastSessionLL.setVisibility(View.GONE);
            holder.patientComplianceLL.setVisibility(View.GONE);
            holder.patientNoPrescriptionTV.setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(selectedPatientName) && selectedPatientName.equals(patientList.get(position).getName())) {
            holder.view.setBackground(context.getResources().getDrawable(R.drawable.shape_card_selected));
            oldView = holder.view;
        } else {
            holder.view.setBackground(context.getResources().getDrawable(R.drawable.shape_card));
        }
    }

    /**
     *
     * @return
     */
    @Override
    public int getItemCount() {
        return patientList.size();
    }

    /**
     * Method used to update the list of patients when filtered by the user
     * @param patients
     */
    public void updatePatientList(List<Patient> patients) {
        this.patientList = patients;
    }

    /**
     * Used to unselect the the a previous selected patient.
     */
    public void unselect() {
        selectedPatientName = null;
        if (oldView != null) {
            oldView.setBackground(context.getResources().getDrawable(R.drawable.shape_card));
            oldView = null;
        }
    }

    /**
     *
     */
    public class PatientViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected CircleImageView patientPhotoCIV;
        protected TextView patientNameTV;
        protected TextView patientAgeTV;
        protected TextView patientComplianceTV;
        protected TextView patientLastSessionTV;
        protected TextView patientNoPrescriptionTV;
        protected LinearLayout patientComplianceLL;
        protected LinearLayout patientLastSessionLL;
        protected View view;

        public PatientViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            itemView.setOnClickListener(this);
            patientPhotoCIV = (CircleImageView) itemView.findViewById(R.id.adapter_patient_photo_iv);
            patientNameTV = (TextView) itemView.findViewById(R.id.adapter_patient_name_tv);
            patientAgeTV = (TextView) itemView.findViewById(R.id.adapter_patient_age_tv);
            patientComplianceTV = (TextView) itemView.findViewById(R.id.adapter_patient_compliance_tv);
            patientLastSessionTV = (TextView) itemView.findViewById(R.id.adapter_patient_last_session_tv);
            patientNoPrescriptionTV = (TextView) itemView.findViewById(R.id.adapter_patient_no_prescription_tv);
            patientComplianceLL = (LinearLayout) itemView.findViewById(R.id.adapter_patient_compliance_ll);
            patientLastSessionLL = (LinearLayout) itemView.findViewById(R.id.adapter_patient_last_session_ll);
        }

        /**
         *
         * @param view
         */
        @Override
        public void onClick(View view) {
            if (oldView != null) {
                oldView.setBackground(context.getResources().getDrawable(R.drawable.shape_card));
            }
            view.setBackground(context.getResources().getDrawable(R.drawable.shape_card_selected));
            oldView = view;
            selectedPatientName = patientList.get(getAdapterPosition()).getName();
            PatientDetailDialogFragment patientDetailDialogFragment = PatientDetailDialogFragment.newInstance(patientList.get(getAdapterPosition()));
            patientDetailDialogFragment.show(((Activity) context).getFragmentManager(), PatientDetailDialogFragment.class.getSimpleName());
        }
    }

}
