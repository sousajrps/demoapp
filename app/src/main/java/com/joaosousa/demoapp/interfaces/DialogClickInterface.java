package com.joaosousa.demoapp.interfaces;

/**
 * Created by jrpso on 07/10/2017.
 */

public interface DialogClickInterface {
    void onDialogClickListener(int buttonType);
}
