package com.joaosousa.demoapp.local;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by jrpso on 06/10/2017.
 */

@Dao
public interface UserDao {

    /**
     * Get entity by username. For this App, we will pass in an ID when the detail Activity starts;
     * therefore we need not use LiveData as the Data will not change during the Activity's
     * Lifecycle.
     *
     * @param username A Unique identifier for a given record within the Database.
     * @return
     */
    @Query("SELECT * FROM User WHERE username = :username")
    LiveData<User> getUserByUsername(String username);

    /**
     * Get all entities of type User
     *
     * @return
     */
    @Query("SELECT * FROM User")
    LiveData<List<User>> getUsers();


    /**
     * Insert a new ListItem
     *
     * @param user
     */
    @Insert(onConflict = REPLACE)
    Long insertUser(User user);

    /**
     *
     * @param user
     */
    @Delete
    void deleteUser(User user);

}
