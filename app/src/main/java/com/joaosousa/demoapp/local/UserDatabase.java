package com.joaosousa.demoapp.local;

/**
 * Created by jrpso on 06/10/2017.
 */

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;


@Database(entities = {User.class}, version = 1)
public abstract class UserDatabase extends RoomDatabase {
    /**
     * Variables
     */
    private static UserDatabase INSTANCE;

    /**
     * @param context
     * @return
     */
    public static UserDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), UserDatabase.class, "DemoAppDB")
                            .build();
        }
        return INSTANCE;
    }

    /**
     *
     */
    public static void destroyInstance() {
        INSTANCE = null;
    }

    /**
     *
     * @return
     */
    public abstract UserDao userDao();

}