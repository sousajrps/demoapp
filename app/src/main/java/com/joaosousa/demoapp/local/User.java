package com.joaosousa.demoapp.local;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by jrpso on 06/10/2017.
 */

@Entity
public class User {

    @PrimaryKey
    @NonNull
    private String username;
    private String name;
    private int photo;

    public User(String username, String name, int photo) {
        this.username = username;
        this.name = name;
        this.photo = photo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }
}
