package com.joaosousa.demoapp.view;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joaosousa.demoapp.R;
import com.joaosousa.demoapp.adapters.PatientAdapter;
import com.joaosousa.demoapp.databinding.FragmentPatientsListBinding;
import com.joaosousa.demoapp.remote.model.Patient;
import com.joaosousa.demoapp.utils.Utils;
import com.joaosousa.demoapp.viewmodel.PatientViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jrpso on 04/10/2017.
 */

public class PatientListFragment extends LifecycleFragment {

    /**
     * Variables
     */
    public static final String TAG = PatientListFragment.class.getSimpleName();
    public static final int HAS_PRESCRITION_TRUE = 1;
    public static final int HAS_PRESCRITION_FALSE = 0;

    private List<Patient> patientList = new ArrayList<>();
    private String username;
    private FragmentPatientsListBinding binding;
    private PatientViewModel viewModel;
    private SwipeRefreshLayout swipeRefreshLayout;
    private PatientAdapter patientAdapter;
    private GridLayoutManager gridLayoutManager;

    /**
     * Views
     */
    private TabLayout tabLayout;
    private View underlineTabLayout;
    private RecyclerView patientListRV;

    /**
     * Listeners
     */
    private TabLayout.OnTabSelectedListener onTabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            patientAdapter.updatePatientList(filterPatientsList(tab.getPosition()));
            patientAdapter.notifyDataSetChanged();
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            observeViewModel(viewModel);
        }
    };

    /**
     *
     * @param username
     * @return
     */
    public static PatientListFragment newInstance(String username) {
        PatientListFragment fragment = new PatientListFragment();
        Bundle args = new Bundle();
        args.putString(LoginActivity.USERNAME_TAG, username);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            username = getArguments().getString(LoginActivity.USERNAME_TAG);
        }

        PatientViewModel.Factory factory = new PatientViewModel.Factory(
                getActivity().getApplication(), username);

        viewModel = ViewModelProviders.of(this, factory)
                .get(PatientViewModel.class);

        observeViewModel(viewModel);
    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_patients_list, container, false);
        binding.setIsLoading(true);
        binding.setNoConnection(false);

        tabLayout = binding.patientListTl;
        underlineTabLayout = binding.patientListTabLayoutUnderlineV;
        patientListRV = binding.patientListRl;
        swipeRefreshLayout = binding.patientListSrl;

        swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
        swipeRefreshLayout.setEnabled(false);
        swipeRefreshLayout.setColorSchemeResources(
                R.color.coral_pink,
                R.color.sorenge,
                R.color.coral_pink);
        gridLayoutManager = new GridLayoutManager(getActivity(), Utils.getNumberOfColumnsForWidth(getActivity(), 150));
        patientListRV.setLayoutManager(gridLayoutManager);
        return binding.getRoot();
    }

    /**
     * Observable for the list of patients
     * @param viewModel
     */
    private void observeViewModel(final PatientViewModel viewModel) {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setEnabled(false);
        }
        // Update the list when the data changes
        viewModel.getPatientListObservable().observe(PatientListFragment.this, new Observer<List<Patient>>() {
            @Override
            public void onChanged(@Nullable List<Patient> patients) {
                binding.setIsLoading(false);
                swipeRefreshLayout.setEnabled(true);
                if (patients != null) {
                    binding.setNoConnection(false);
                    patientList = patients;
                    if (tabLayout.getTabCount() == 0) {
                        initializeTabLayout();
                    }
                    patientAdapter = new PatientAdapter(getActivity(), filterPatientsList(tabLayout.getSelectedTabPosition()), null);
                    patientListRV.setAdapter(patientAdapter);
                    swipeRefreshLayout.setRefreshing(false);


                } else {
                    swipeRefreshLayout.setRefreshing(false);
                    binding.setNoConnection(true);
                }
            }
        });
    }

    /**
     * Initialization of the Tablayout according to the list of patients
     */
    private void initializeTabLayout() {
        underlineTabLayout.setVisibility(View.VISIBLE);
        View v1 = LayoutInflater.from(getContext()).inflate(R.layout.tablayout_custom_view, null);
        TextView tv1 = (TextView) v1.findViewById(R.id.tab_layout_custom_view_tv);
        tv1.setText(getString(R.string.patient_list_all) + " / " + String.valueOf(patientListSize(0)));
        tabLayout.addTab(tabLayout.newTab().setCustomView(v1));
        tabLayout.getTabAt(0).select();

        View v2 = LayoutInflater.from(getContext()).inflate(R.layout.tablayout_custom_view, null);
        TextView tv2 = (TextView) v2.findViewById(R.id.tab_layout_custom_view_tv);
        tv2.setText(getString(R.string.patient_list_ongoing) + " / " + String.valueOf(patientListSize(1)));
        tabLayout.addTab(tabLayout.newTab().setCustomView(v2));

        View v3 = LayoutInflater.from(getContext()).inflate(R.layout.tablayout_custom_view, null);
        TextView tv3 = (TextView) v3.findViewById(R.id.tab_layout_custom_view_tv);
        tv3.setText(getString(R.string.patient_list_no_program) + " / " + String.valueOf(patientListSize(2)));
        tabLayout.addTab(tabLayout.newTab().setCustomView(v3));
        tabLayout.addOnTabSelectedListener(onTabSelectedListener);
    }

    /**
     *
     * @param position
     * @return
     */
    private List<Patient> filterPatientsList(int position) {
        List<Patient> patientsFiltered = new ArrayList<>();
        switch (position) {
            case 0:
                patientsFiltered = patientList;
                break;
            case 1:
                for (Patient p : patientList) {
                    if (p.getHasPrescription() == HAS_PRESCRITION_TRUE) {
                        patientsFiltered.add(p);
                    }
                }
                break;
            case 2:
                for (Patient p : patientList) {
                    if (p.getHasPrescription() == HAS_PRESCRITION_FALSE) {
                        patientsFiltered.add(p);
                    }
                }
                break;
        }
        return patientsFiltered;
    }

    /**
     *
     * @param position
     * @return
     */
    private int patientListSize(int position) {
        return filterPatientsList(position).size();
    }

    /**
     * Updated the GridLayout when the orientation changes
     * @param newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        gridLayoutManager = new GridLayoutManager(getActivity(), Utils.getNumberOfColumnsForWidth(getActivity(), 150));
        patientListRV.setLayoutManager(gridLayoutManager);
    }

}
