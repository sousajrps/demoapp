package com.joaosousa.demoapp.view;

import android.app.AlertDialog;
import android.arch.lifecycle.LifecycleActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.joaosousa.demoapp.R;
import com.joaosousa.demoapp.interfaces.DialogClickInterface;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by jrpso on 04/10/2017.
 */

public class PrivateActivity extends LifecycleActivity implements DialogClickInterface {
    /**
     * Views
     */
    private CircleImageView userPhotoIv;
    private ImageView settingsIV;
    private TextView userNameTV;

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generic);
        userPhotoIv = (CircleImageView) findViewById(R.id.toolbar_photo_iv);
        userNameTV = (TextView) findViewById(R.id.toolbar_username_tv);
        settingsIV = (ImageView) findViewById(R.id.toolbar_settings_iv);
        settingsIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLogoutDialog();
            }
        });
    }

    /**
     *
     * @param doctorName
     */
    void setToolbarUserName(String doctorName) {
        userNameTV.setText(doctorName);
    }

    void setToolbarUserPhoto(Integer photo) {
        if (photo != null) {
            userPhotoIv.setImageResource(photo);
        }
    }


    /**
     *
     */
    @Override
    public void onBackPressed() {
        showLogoutDialog();
    }

    private void showLogoutDialog() {
        WarningDialogFragment warningDialogFragment =
                WarningDialogFragment.newInstance(
                        getString(R.string.alert_dialog_warning),
                        getString(R.string.alert_dialog_logout_warning));
        warningDialogFragment.setDialogClickInterface(this);
        warningDialogFragment.show(getFragmentManager(), WarningDialogFragment.class.getSimpleName());
    }

    /**
     *
     * @param buttonType
     */
    @Override
    public void onDialogClickListener(int buttonType) {
        if (buttonType == AlertDialog.BUTTON_POSITIVE) {
            Intent intent = new Intent(PrivateActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
