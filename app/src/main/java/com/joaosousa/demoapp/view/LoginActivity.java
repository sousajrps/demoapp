package com.joaosousa.demoapp.view;

import android.arch.lifecycle.LifecycleActivity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joaosousa.demoapp.R;
import com.joaosousa.demoapp.local.User;
import com.joaosousa.demoapp.viewmodel.UserViewModel;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class LoginActivity extends LifecycleActivity {

    /**
     * Variables
     */
    public static String USERNAME_TAG = "username";
    public static String NAME_TAG = "name";
    public static String PHOTO_TAG = "photo";

    private String username;
    private String password;
    private UserViewModel userViewModel;
    private User user;

    /**
     * Views
     */
    private EditText usernameEt;
    private EditText passwordEt;
    private TextView welcomeTv;
    private TextView changeUserTv;
    private CircleImageView photoCiv;
    private LinearLayout userLl;


    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        usernameEt = (EditText) findViewById(R.id.login_username_et);
        passwordEt = (EditText) findViewById(R.id.login_password_et);
        photoCiv = (CircleImageView) findViewById(R.id.login_user_photo_civ);
        userLl = (LinearLayout) findViewById(R.id.login_user_ll);
        welcomeTv = (TextView) findViewById(R.id.login_user_welcome_tv);
        changeUserTv = (TextView) findViewById(R.id.login_change_user_tv);
        changeUserTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user != null) {
                    userViewModel.deleteUser(user);
                }
            }
        });

        TextInputLayout usernameTIL = (TextInputLayout) findViewById(R.id.login_username_til);
        TextInputLayout passwordTIL = (TextInputLayout) findViewById(R.id.login_password_til);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/AvenirLTStd-Roman.otf");
        usernameTIL.setTypeface(font);
        passwordTIL.setTypeface(font);


        Button button = (Button) findViewById(R.id.login_bt);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (usernameEt.getVisibility() == View.VISIBLE) {
                    username = usernameEt.getText().toString().trim();
                }
                password = passwordEt.getText().toString().trim();
                if (validateFields(username, password)) {
                    doLogin(username, password);
                }
            }
        });

        /**
         * Check if there is any user previously logged
         */
        userViewModel = ViewModelProviders.of(this)
                .get(UserViewModel.class);
        observeGetUsersViewModel(userViewModel);
    }

    /**
     * Validates if the mandatory fields are correctly filled
     * @param username
     * @param password
     * @return
     */
    private boolean validateFields(String username, String password) {
        boolean isValid = true;

        if (TextUtils.isEmpty(username)) {
            isValid = false;
            usernameEt.setError(getString(R.string.login_username_error));
        }

        if (TextUtils.isEmpty(password)) {
            isValid = false;
            passwordEt.setError(getString(R.string.login_password_error));
        }

        return isValid;
    }

    /**
     * Method that simulates the login procedure and stores the login information
     * @param username
     * @param password
     */
    private void doLogin(String username, String password) {
        User user = new User(username, "Dr. John Anderson", R.drawable.doctor);
        userViewModel.insertUser(user);

        Intent intent = new Intent(LoginActivity.this, PatientsListActivity.class);
        intent.putExtra(USERNAME_TAG, user.getUsername());
        intent.putExtra(NAME_TAG, user.getName());
        intent.putExtra(PHOTO_TAG, user.getPhoto());

        startActivity(intent);
        finish();
    }

    /**
     * Observable for the stored user
     *
     * @param viewModel
     */
    private void observeGetUsersViewModel(final UserViewModel viewModel) {
        // Update the list when the data changes
        viewModel.getUsers().observe(LoginActivity.this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable List<User> users) {

                if (users != null && users.size() > 0) {
                    user = users.get(0);
                    userLl.setVisibility(View.VISIBLE);
                    changeUserTv.setVisibility(View.VISIBLE);
                    usernameEt.setVisibility(View.GONE);
                    photoCiv.setImageResource(users.get(0).getPhoto());
                    welcomeTv.setText(getString(R.string.login_welcome) + " " + users.get(0).getName());
                    username = users.get(0).getUsername();

                } else {
                    userLl.setVisibility(View.GONE);
                    changeUserTv.setVisibility(View.GONE);
                    usernameEt.setVisibility(View.VISIBLE);

                }
            }
        });
    }
}
