package com.joaosousa.demoapp.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.joaosousa.demoapp.R;

public class PatientsListActivity extends PrivateActivity {

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent() != null) {
            String username = getIntent().getStringExtra(LoginActivity.USERNAME_TAG);
            String name = getIntent().getStringExtra(LoginActivity.NAME_TAG);
            Integer photo = getIntent().getIntExtra(LoginActivity.PHOTO_TAG, R.drawable.doctor);
            if (!TextUtils.isEmpty(name)) {
                setToolbarUserName(name);
            }
            setToolbarUserPhoto(photo);

            if (savedInstanceState == null) {
                PatientListFragment fragment = PatientListFragment.newInstance(username);
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragment_container, fragment, PatientListFragment.TAG).commit();
            }
        }
    }

}
