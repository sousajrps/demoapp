package com.joaosousa.demoapp.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.joaosousa.demoapp.R;
import com.joaosousa.demoapp.interfaces.DialogClickInterface;

import javax.annotation.Nullable;

/**
 * Created by jrpso on 06/10/2017.
 */

public class WarningDialogFragment extends DialogFragment {
    /**
     * Variables
     */
    public static final String TITLE_TAG = "title";
    public static final String MESSAGE_TAG = "message";

    /**
     * Listeners
     */
    private DialogClickInterface dialogClickInterface;

    /**
     *
     */
    public WarningDialogFragment() {

    }

    /**
     * @param title
     * @param message
     * @return
     */
    public static WarningDialogFragment newInstance(String title, String message) {
        WarningDialogFragment frag = new WarningDialogFragment();
        Bundle args = new Bundle();
        args.putString(TITLE_TAG, title);
        args.putString(MESSAGE_TAG, message);
        frag.setArguments(args);
        return frag;
    }

    /**
     *
     * @param savedInstanceState
     * @return
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_frament_warning, container);
    }

    /**
     *
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String title = getArguments().getString(TITLE_TAG);
        String message = getArguments().getString(MESSAGE_TAG);

        TextView titleTv = (TextView) view.findViewById(R.id.dialog_fragment_warning_title_tv);
        titleTv.setText(title);

        TextView messageTv = (TextView) view.findViewById(R.id.dialog_fragment_warning_message_tv);
        messageTv.setText(message);

        Button yesBt = (Button) view.findViewById(R.id.dialog_fragment_warning_yes_bt);
        yesBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialogClickInterface != null) {
                    dialogClickInterface.onDialogClickListener(AlertDialog.BUTTON_POSITIVE);
                }
                dismiss();
            }
        });
        Button cancelBt = (Button) view.findViewById(R.id.dialog_fragment_warning_cancel_bt);
        cancelBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialogClickInterface != null) {
                    dialogClickInterface.onDialogClickListener(AlertDialog.BUTTON_NEGATIVE);
                }
                dismiss();
            }
        });


    }

    /**
     *
     */
    @Override
    public void onStart() {
        super.onStart();
        setWindowProperties();
    }

    /**
     *
     * @param newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setWindowProperties();
    }

    /**
     *
     */
    private void setWindowProperties() {
        Dialog dialog = getDialog();

        if (dialog != null) {
            dialog.setCancelable(true);
            Window window = dialog.getWindow();
            Display display = window.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            Double factor = 0.0;
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                factor = 0.75;
            } else {
                factor = 0.5;
            }
            int windowWidth = ((Double) (size.x * factor)).intValue();
            int windowHeight = ((Double) (size.y * factor)).intValue();
            window.setLayout(windowWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
            window.setDimAmount(0.7f);
        }
    }

    /**
     *
     * @param dialogClickInterface
     */
    public void setDialogClickInterface(DialogClickInterface dialogClickInterface) {
        this.dialogClickInterface = dialogClickInterface;
    }
}
