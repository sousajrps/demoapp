package com.joaosousa.demoapp.view;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.joaosousa.demoapp.R;
import com.joaosousa.demoapp.remote.model.Patient;
import com.joaosousa.demoapp.utils.Utils;

import javax.annotation.Nullable;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by jrpso on 06/10/2017.
 */

public class PatientDetailDialogFragment extends DialogFragment {
    /**
     * Variables
     */
    public static final String PATIENT_TAG = "patient";

    /**
     *
     */
    public PatientDetailDialogFragment() {

    }

    /**
     * @param patient
     * @return
     */
    public static PatientDetailDialogFragment newInstance(Patient patient) {
        PatientDetailDialogFragment frag = new PatientDetailDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable(PATIENT_TAG, patient);
        frag.setArguments(args);
        return frag;
    }

    /**
     *
     * @param savedInstanceState
     * @return
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    /**
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_frament_patient_detail, container);
    }

    /**
     *
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView nameTv = (TextView) view.findViewById(R.id.patient_detail_name_tv);
        TextView birthDateTv = (TextView) view.findViewById(R.id.patient_detail_birth_date_tv);
        TextView pathologyTv = (TextView) view.findViewById(R.id.patient_detail_pathology_tv);
        CircleImageView photoCiv = (CircleImageView) view.findViewById(R.id.patient_detail_photo_civ);
        Button closeBt = (Button) view.findViewById(R.id.patient_detail_close_bt);
        closeBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        Patient patient = (Patient) getArguments().getSerializable(PATIENT_TAG);
        if (patient != null) {
            nameTv.setText(patient.getName());
            birthDateTv.setText(patient.getBirthDate());
            pathologyTv.setText(patient.getPathology());
            photoCiv.setImageResource(Utils.getPatientPhoto(patient.getName()));
        }
    }

    /**
     *
     */
    @Override
    public void onStart() {
        super.onStart();
        setWindowProperties();
    }

    /**
     *
     * @param newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setWindowProperties();
    }

    /**
     *
     */
    private void setWindowProperties() {
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.setCancelable(true);
            Window window = dialog.getWindow();
            Display display = window.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            Double factor = 0.0;
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                factor = 0.75;
            } else {
                factor = 0.5;
            }
            int windowWidth = ((Double) (size.x * factor)).intValue();
            int windowHeight = ((Double) (size.y * factor)).intValue();
            window.setLayout(windowWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
            window.setDimAmount(0.7f);
        }
    }
}
