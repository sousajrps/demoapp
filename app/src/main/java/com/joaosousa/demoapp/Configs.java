package com.joaosousa.demoapp;

/**
 * Created by jrpso on 05/10/2017.
 */

public class Configs {
    //Default font to be used across de app
    public static final String DEFAULT_FONT = "fonts/AvenirLTStd-Roman.otf";

    //Base URL for REST API connection
    public static final String BASE_URL = "https://api.myjson.com/";

    // Directory used to store cache files
    public static final String CACHE_FILE = "DemoApp";

    // Cache size in MB
    public static final int CACHE_SIZE = 10 * 1024 * 1024;

    // Connection timeout duration
    public static final int TIMEOUT_CONNECTION = 60000;

    // Connection timeout duration
    public static final int TIMEOUT_READ = 30000;
}
