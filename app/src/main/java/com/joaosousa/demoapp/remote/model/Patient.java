package com.joaosousa.demoapp.remote.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by jrpso on 03/10/2017.
 */

public class Patient implements Serializable {

    @SerializedName("name")
    private String name;

    @SerializedName("birthDate")
    private String birthDate;

    @SerializedName("pathology")
    private String pathology;

    @SerializedName("hasPrescription")
    private int hasPrescription;

    @SerializedName("compliance")
    private String compliance;

    @SerializedName("lastSession")
    private String lastSession;

    public Patient(String name, String birthDate, String pathology, int hasPrescription,
                   String compliance, String lastSession) {
        this.name = name;
        this.birthDate = birthDate;
        this.pathology = pathology;
        this.hasPrescription = hasPrescription;
        this.compliance = compliance;
        this.lastSession = lastSession;
    }

    public Patient() {
        this.name = null;
        this.birthDate = null;
        this.pathology = null;
        this.hasPrescription = 0;
        this.compliance = null;
        this.lastSession = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getPathology() {
        return pathology;
    }

    public void setPathology(String pathology) {
        this.pathology = pathology;
    }

    public int getHasPrescription() {
        return hasPrescription;
    }


    public void setHasPrescription(int hasPrescription) {
        this.hasPrescription = hasPrescription;
    }

    public String getCompliance() {
        return compliance;
    }

    public void setCompliance(String compliance) {
        this.compliance = compliance;
    }

    public String getLastSession() {
        return lastSession;
    }

    public void setLastSession(String lastSession) {
        this.lastSession = lastSession;
    }

}