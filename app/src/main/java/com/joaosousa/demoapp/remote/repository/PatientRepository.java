package com.joaosousa.demoapp.remote.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.joaosousa.demoapp.ApplicationClass;
import com.joaosousa.demoapp.BuildConfig;
import com.joaosousa.demoapp.Configs;
import com.joaosousa.demoapp.remote.model.Patient;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static okhttp3.logging.HttpLoggingInterceptor.Level.HEADERS;
import static okhttp3.logging.HttpLoggingInterceptor.Level.NONE;

/**
 * Created by jrpso on 03/10/2017.
 */


public class PatientRepository {
    /**
     * Variables
     */
    public static final String TAG = PatientRepository.class.getSimpleName();
    private static final String CACHE_CONTROL = "Cache-Control";
    private static PatientRepository patientRepository;
    private PatientService patientService;

    /**
     *
     */
    private PatientRepository() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Configs.BASE_URL)
                .client(provideOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        patientService = retrofit.create(PatientService.class);
    }

    /**
     * @return
     */
    private static OkHttpClient provideOkHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(provideHttpLoggingInterceptor())
                .addInterceptor(provideOfflineCacheInterceptor())
                .addNetworkInterceptor(provideCacheInterceptor())
                .cache(provideCache())
                .readTimeout(Configs.TIMEOUT_READ, TimeUnit.MILLISECONDS)
                .connectTimeout(Configs.TIMEOUT_CONNECTION, TimeUnit.MILLISECONDS)
                .build();
    }

    /**
     *
     * @return
     */
    private static Cache provideCache() {
        Cache cache = null;
        try {
            /** Set the cache properties **/
            cache = new Cache(new File(ApplicationClass.getInstance().getCacheDir(), Configs.CACHE_FILE),
                    Configs.CACHE_SIZE); // 10 MB
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return cache;
    }

    /**
     *
     * @return
     */
    private static HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor =
                new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                    @Override
                    public void log(String message) {
                        Log.e(TAG, message);
                    }
                });
        httpLoggingInterceptor.setLevel(BuildConfig.DEBUG ? HEADERS : NONE);
        return httpLoggingInterceptor;
    }

    /**
     *
     * @return
     */
    public static Interceptor provideOfflineCacheInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();

                if (!ApplicationClass.hasNetwork()) {
                    CacheControl cacheControl = new CacheControl.Builder()
                            .maxStale(7, TimeUnit.DAYS)
                            .build();

                    request = request.newBuilder()
                            .cacheControl(cacheControl)
                            .build();
                }

                return chain.proceed(request);
            }
        };
    }

    /**
     *
     * @return
     */
    public static Interceptor provideCacheInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());

                // re-write response header to force use of cache
                CacheControl cacheControl = new CacheControl.Builder()
                        .maxAge(2, TimeUnit.MINUTES)
                        .build();

                return response.newBuilder()
                        .header(CACHE_CONTROL, cacheControl.toString())
                        .build();
            }
        };
    }

    /**
     *
     * @return
     */
    public synchronized static PatientRepository getInstance() {
        if (patientRepository == null) {
            patientRepository = new PatientRepository();
        }
        return patientRepository;
    }

    /**
     *
     * @param user
     * @return
     */
    public LiveData<List<Patient>> getPatientList(String user) {
        final MutableLiveData<List<Patient>> data = new MutableLiveData<>();

        patientService.getPatientsList().enqueue(new Callback<List<Patient>>() {
            @Override
            public void onResponse(retrofit2.Call<List<Patient>> call, retrofit2.Response<List<Patient>> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(retrofit2.Call<List<Patient>> call, Throwable t) {
                data.setValue(null);
            }
        });

        return data;
    }
}