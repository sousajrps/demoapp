package com.joaosousa.demoapp.remote.repository;

import com.joaosousa.demoapp.remote.model.Patient;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by jrpso on 03/10/2017.
 */

public interface PatientService {

    @GET("bins/1gevd1")
    Call<List<Patient>> getPatientsList();
}
